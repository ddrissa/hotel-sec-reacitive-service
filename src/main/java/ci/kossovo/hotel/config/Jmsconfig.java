package ci.kossovo.hotel.config;

import javax.jms.ConnectionFactory;

import org.springframework.boot.autoconfigure.jms.DefaultJmsListenerContainerFactoryConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.jms.support.converter.MessageType;

@Configuration
public class Jmsconfig {
	@Bean
	public JmsListenerContainerFactory<?> myFactory(ConnectionFactory connectionFactory,
			DefaultJmsListenerContainerFactoryConfigurer configurer) {

		//Ceci fournit toutes les valeurs par défaut d'amorçage à cette fabrique, y compris le convertisseur de message
		DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
		// Vous pouvez toujours remplacer certaines valeurs par défaut de Boot si nécessaireF
		configurer.configure(factory, connectionFactory);
		return factory;

	}
	//Sérialiser le contenu du message en json en utilisant TextMessage
	@Bean
	public MessageConverter	 jacksonJmsMessageConverter() {
		MappingJackson2MessageConverter converter = new MappingJackson2MessageConverter();
		converter.setTargetType(MessageType.TEXT);
		converter.setTypeIdPropertyName("_type");
		
		return converter;
	}

}
