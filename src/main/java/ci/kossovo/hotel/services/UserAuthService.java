package ci.kossovo.hotel.services;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import ci.kossovo.hotel.repository.AppUserRepository;
import ci.kossovo.hotel.securite.UserPrincipal;
import reactor.core.publisher.Mono;

@Service
public class UserAuthService{
	
	private AppUserRepository userRepository;
	
	public UserAuthService(AppUserRepository userRepository) {
		this.userRepository = userRepository;
	}

	
	public Mono<UserDetails> loadUserByUsername(String usernameOrEmail) {
		return userRepository.findByUsernameOrEmail(usernameOrEmail, usernameOrEmail)
		 .map( user -> UserPrincipal.create(user));	
	}
	
	// Cette méthode est utilisée par JWTAuthenticationFilter
	public Mono<UserDetails> loadUserById(String id) throws UsernameNotFoundException {
		return userRepository.findById(id).map(user -> UserPrincipal.create(user));		
		
	}

}
