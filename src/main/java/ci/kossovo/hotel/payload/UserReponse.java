package ci.kossovo.hotel.payload;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
@AllArgsConstructor
public class UserReponse {
	private String id;
	private String titulaire;
	private String name;
	private String username;
	private String email;
	private List<String> roles= new ArrayList<>();
	private boolean accountNonExpired = true;

	private boolean accountNonLocked = true;

	private boolean credentialsNonExpired = true;

	private boolean enabled = true;
	
}
