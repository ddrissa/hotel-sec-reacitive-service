package ci.kossovo.hotel.payload;

import javax.validation.constraints.NotBlank;

import lombok.Getter;
import lombok.Setter;

public class LoginRequest {

	@NotBlank(message="Login est obligatoire.")
	@Getter @Setter
	private String usernameOrEmail;
	@NotBlank(message="Le mot de passe est obligatoire.")
	@Getter @Setter
	private String password;
}
