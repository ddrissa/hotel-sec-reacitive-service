package ci.kossovo.hotel.payload;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Setter @Getter
@AllArgsConstructor
public class ApiReponse {
	
	private Boolean success;
	private String message;

}
