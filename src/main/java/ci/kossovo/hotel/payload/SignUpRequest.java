package ci.kossovo.hotel.payload;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter @Getter
@ToString
public class SignUpRequest {
	
	@NotBlank(message="Nom est obligatoire.")
	@Size(min=4, max=40)
	private String name;
	
	@NotBlank(message="La cni est obligatoire.")
	private String cni;
	
	@NotBlank(message="Username est obligatoire.")
	@Size(min=3, max=20)
	private String username;
	
	@NotBlank(message="Enail est obligatoire.")
	@Size(max=40)
	@Email(message="L'email est mal forme.")
	private String email;
	
	@NotBlank(message="Password est obligatoire.")
	@Size(min=4, max=40)
	private String password;
	@NotBlank(message="Il faut confirmer le password.")
	@Size(min=4, max=40)
	private String confirmPassword;

}
