package ci.kossovo.hotel;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.reactive.config.EnableWebFlux;

import ci.kossovo.hotel.documents.AppRole;
import ci.kossovo.hotel.documents.num.NomRole;
import ci.kossovo.hotel.repository.AppRoleRepository;
import reactor.core.publisher.Flux;

@SpringBootApplication
@EnableWebFlux
public class HotelSecuriteReactiveServiceApplication implements CommandLineRunner {
	private AppRoleRepository appRoleRepository;
	
	

	public HotelSecuriteReactiveServiceApplication(AppRoleRepository appRoleRepository) {
		this.appRoleRepository = appRoleRepository;
	}

	public static void main(String[] args) {
		SpringApplication.run(HotelSecuriteReactiveServiceApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		
		
		appRoleRepository.deleteAll().thenMany(
				 Flux.just(
							new AppRole(NomRole.ROLE_USER),
							new AppRole(NomRole.ROLE_ADMIN),
							new AppRole(NomRole.ROLE_CLIENT),
							new AppRole(NomRole.ROLE_DBA),
							new AppRole(NomRole.ROLE_EMPLOYE),
							new AppRole(NomRole.ROLE_GERANT)
							)
							.flatMap(appRoleRepository:: save)
					)
			.thenMany(appRoleRepository.findAll())
			.subscribe(System.out :: println);
		
		/*
		 * Flux<AppRole> roleflux= Flux.just( new AppRole(NomRole.ROLE_USER), new
		 * AppRole(NomRole.ROLE_ADMIN), new AppRole(NomRole.ROLE_CLIENT), new
		 * AppRole(NomRole.ROLE_DBA), new AppRole(NomRole.ROLE_EMPLOYE), new
		 * AppRole(NomRole.ROLE_GERANT) ) .flatMap(appRoleRepository:: save); roleflux
		 * .thenMany(appRoleRepository.findAll()) .subscribe(System.out :: println);
		 */
		
	}

}
