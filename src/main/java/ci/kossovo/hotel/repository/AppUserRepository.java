package ci.kossovo.hotel.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

import ci.kossovo.hotel.documents.AppUser;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface AppUserRepository extends ReactiveMongoRepository<AppUser, String> {

	 Flux<AppUser> findBy();

	 Mono<AppUser>findByUsernameOrEmail(String username, String email);

	Flux<AppUser> findByIdIn(List<String> userIds);

	Mono<AppUser> findByUsername(String username);

	Mono<AppUser> findByEmail(String mail);

	Mono<AppUser> findByTitulaire(String code);

	Mono<Boolean> existsByUsername(String username);
	Mono<Boolean> existsByUsernameOrEmail(String username, String email);

	Mono<Boolean> existsByEmail(String email);
	
	

}
