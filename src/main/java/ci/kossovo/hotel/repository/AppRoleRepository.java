package ci.kossovo.hotel.repository;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

import ci.kossovo.hotel.documents.AppRole;
import ci.kossovo.hotel.documents.num.NomRole;
import reactor.core.publisher.Mono;

public interface AppRoleRepository extends ReactiveMongoRepository<AppRole,	String> {

	Mono<Boolean> existsByNom(NomRole nom);
	Mono<AppRole> findByNom(NomRole nom);
}
