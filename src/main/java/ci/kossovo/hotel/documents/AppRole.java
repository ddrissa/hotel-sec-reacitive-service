package ci.kossovo.hotel.documents;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import ci.kossovo.hotel.documents.num.NomRole;

@Document
public class AppRole {

	@Id
	private String id;
	@Indexed
	private NomRole nom;
	private String desc;

	public AppRole() {

	}

	public AppRole(NomRole nom) {
		this.nom = nom;
	}

	public AppRole(NomRole nom, String desc) {
		this.nom = nom;
		this.desc = desc;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public NomRole getNom() {
		return nom;
	}

	public void setNom(NomRole nom) {
		this.nom = nom;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	@Override
	public String toString() {
		return "Role [id=" + id + ", nomRole=" + nom + ", desc=" + desc + "]";
	}

}
