package ci.kossovo.hotel.documents.num;

public enum NomRole {
	ROLE_USER,
    ROLE_ADMIN,
    ROLE_EMPLOYE,
    ROLE_CLIENT,
    ROLE_GERANT,
    ROLE_DBA

}
