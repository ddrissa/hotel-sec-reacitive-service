package ci.kossovo.hotel.securite;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.security.web.server.context.ServerSecurityContextRepository;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.server.ServerWebExchange;

import ci.kossovo.hotel.outils.ConstantSecurite;
import reactor.core.publisher.Mono;

@Component
public class MySecurityContextRepository implements ServerSecurityContextRepository {
	
	@Autowired
	private MyAuthenticationManager authenticationManager;

	@Override
	public Mono<Void> save(ServerWebExchange exchange, SecurityContext context) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public Mono<SecurityContext> load(ServerWebExchange exchange) {
		
		String token= getJwtFromRequest(exchange);
		if (Objects.nonNull(token)) {
			Authentication auth = new UsernamePasswordAuthenticationToken(token, token);
			return this.authenticationManager.authenticate(auth).map((authentication) -> {
				return new SecurityContextImpl(authentication);
			});
		}
		return Mono.empty();
	}
	
	
	
	// Methode privee
	private String getJwtFromRequest(ServerWebExchange  swe) {
		ServerHttpRequest request = swe.getRequest();
		
		String hotelToken = request.getHeaders().getFirst(ConstantSecurite.TOKEN_HEADER);
		if (StringUtils.hasText(hotelToken) && hotelToken.startsWith(ConstantSecurite.TOKEN_PREFIX)) {
			return hotelToken.substring(6, hotelToken.length());
		}

		return null;
	}

}
