package ci.kossovo.hotel.securite;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import reactor.core.publisher.Mono;

@Component
public class MyAuthenticationManager implements ReactiveAuthenticationManager {
	@Autowired
	private JwtTokenProvider jwtTokenProvider;
	@Autowired
	private PersoUserDetailsService persoUserDetailsService;

	@Override
	public Mono<Authentication> authenticate(Authentication authentication) {
		String authtoken= authentication.getCredentials().toString();
		
		String userId;
		try {
			userId= jwtTokenProvider.getUserIdFromJWT(authtoken);
		} catch (Exception e) {
			userId=null;
		}
		
		if (Objects.nonNull(userId) && jwtTokenProvider.validateToken(authtoken)) {
			
			UserDetails userDetails = persoUserDetailsService.loadUserById(userId).block();
			
			UsernamePasswordAuthenticationToken auth= new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
			return Mono.just(auth);
		}
		
		return Mono.empty();
	}

}
