package ci.kossovo.hotel.securite;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import ci.kossovo.hotel.repository.AppUserRepository;
import reactor.core.publisher.Mono;

@Service
public class PersoUserDetailsService {

	private AppUserRepository userRepository;

	public PersoUserDetailsService(AppUserRepository userRepository) {
		this.userRepository = userRepository;
	}

	public Mono<UserDetails> findByUsername(String usernameOrEmail) {
		return userRepository.findByUsernameOrEmail(usernameOrEmail, usernameOrEmail).switchIfEmpty(Mono.defer(() -> {
			return Mono.error(new UsernameNotFoundException(
					"Utilisateur non trouvé avec nom d'utilisateur ou email: " + usernameOrEmail));
		})).map(user -> UserPrincipal.create(user));
	}

	public Mono<UserDetails> loadUserById(String id) {
		return userRepository.findById(id).switchIfEmpty(Mono.defer(() -> {
			return Mono.error(new UsernameNotFoundException("Pas de utilisateur " + id));
		})).map(user -> UserPrincipal.create(user));
	}

}
