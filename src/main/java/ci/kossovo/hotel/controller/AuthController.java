package ci.kossovo.hotel.controller;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ci.kossovo.hotel.documents.AppRole;
import ci.kossovo.hotel.documents.AppUser;
import ci.kossovo.hotel.documents.num.NomRole;
import ci.kossovo.hotel.payload.LoginRequest;
import ci.kossovo.hotel.payload.SignUpRequest;
import ci.kossovo.hotel.repository.AppRoleRepository;
import ci.kossovo.hotel.repository.AppUserRepository;
import ci.kossovo.hotel.securite.JwtTokenProvider;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api/auth")
public class AuthController {

	private AppUserRepository userRepository;
	private AppRoleRepository roleRepository;
	private PasswordEncoder passwordEncoder;
	private JwtTokenProvider jwtTokenProvider;

	public AuthController(AppUserRepository userRepository, AppRoleRepository roleRepository,
			PasswordEncoder passwordEncoder, JwtTokenProvider jwtTokenProvider) {
		this.userRepository = userRepository;
		this.roleRepository = roleRepository;
		this.passwordEncoder = passwordEncoder;
		this.jwtTokenProvider = jwtTokenProvider;
	}

// Le corps

	@PostMapping("/signin")
	public ResponseEntity<?> authentifierUser(@Valid @RequestBody LoginRequest loginRequest) {
		return null;
	}

	// inscription

	/*
	 * @PostMapping("/inscrire") public Mono<ResponseEntity<?>>
	 * enregistrerUser(@Valid @RequestBody SignUpRequest signUpRequest) {
	 * 
	 * 
	 * return null ; }
	 */

	@PostMapping("/signup")
	public Mono<ResponseEntity<AppUser>> inscrireUser(@Valid @RequestBody SignUpRequest signUpRequest) {
		
		return roleRepository.findByNom(NomRole.ROLE_USER).flatMap(r -> {
			return Mono.just(r);
		}).flatMap(role -> {

			AppUser appUser = new AppUser(signUpRequest.getCni(), signUpRequest.getName(), signUpRequest.getUsername(),
					signUpRequest.getEmail(), passwordEncoder.encode(signUpRequest.getPassword()));
			appUser.setRoles(Collections.singleton(role));
			return userRepository.save(appUser);

		}).map(ResponseEntity::ok);

	}

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
		Map<String, String> errors = new HashMap<>();

		ex.getBindingResult().getAllErrors().forEach((error) -> {
			String fieldname = ((FieldError) error).getField();
			String errorMessage = error.getDefaultMessage();
			errors.put(fieldname, errorMessage);
		});

		return errors;

	}
}
